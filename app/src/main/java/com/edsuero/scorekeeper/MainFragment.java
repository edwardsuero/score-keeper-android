package com.edsuero.scorekeeper;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by ESuero on 7/14/2015.
 */
public class MainFragment extends Fragment {

    /**
     * Data Members
     */
    private static final String TAG = "mainFragment";
    MyRecyclerAdapter recyclerAdapter1, recyclerAdapter2;
    RecyclerView recyclerView1, recyclerView2;
    LinearLayoutManager linearLayoutManager1, linearLayoutManager2;
    Observer observer1, observer2;
    TextView totalText1, totalText2;
    Team team1;
    Team team2;
    int team1Total, team2Total;
    private EditText teamNameEditText1, teamNameEditText2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        team1 = new Team(getString(R.string.default_team_1_name));
        team2 = new Team(getString(R.string.default_team_2_name));
        team1Total = team1.getTotalScore();
        team2Total = team2.getTotalScore();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.recycle_view_layout, container, false);
        initializeRecycleViews(rootView);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.reset_game) {
            resetGame();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onPause() {
        super.onPause();

        setTeamSettings(Integer.toString(team1.getTotalScore()), "total_1");
        setTeamSettings(Integer.toString(team2.getTotalScore()), "total_2");

        setTeamSettings(teamNameEditText1.getText().toString(), "team_1");
        setTeamSettings(teamNameEditText2.getText().toString(), "team_2");

        setListSettings(team1.getRoundData(), "list1,");
        setListSettings(team2.getRoundData(), "list2,");
    }

    public void onResume() {
        super.onResume();

        if (team1.getTotalScore() != 0) {
            team1.setTotalScore(Integer.parseInt(getTeamSettings("total_1")));
            totalText1.setText(getTeamSettings("total_1"));
        }
        if (team2.getTotalScore() != 0) {
            team2.setTotalScore(Integer.parseInt(getTeamSettings("total_2")));
            totalText2.setText(getTeamSettings("total_2"));
        }

        teamNameEditText1.setText(getTeamSettings("team_1"));
        teamNameEditText2.setText(getTeamSettings("team_2"));

        ArrayList<Integer> list = getListSettings("list1,");
        if (list.size() != 0) {
            if (list.get(0) != 0) {
                team1.setRoundData(list);
                for (int i = 0; i < list.size(); i++)
                    recyclerAdapter1 = new MyRecyclerAdapter(team1, R.layout.item_layout);
                recyclerView1.setAdapter(recyclerAdapter1);

            }
        }

        list = getListSettings("list2,");
        if (list.size() != 0) {
            if (list.get(0) != 0) {
                team2.setRoundData(list);
                recyclerAdapter2 = new MyRecyclerAdapter(team2, R.layout.item_layout);
                recyclerView2.setAdapter(recyclerAdapter2);
            }
        }

    }


    /**
     * Helper Functions
     */

    private void resetGame() {
        /*
         * Reinitialize the ArrayLists for both teams.
         */

        team1.clearList();
        team1.addListItem();
        team1.setTotalScore(0);
        teamNameEditText1.setFocusable(true);
        setListSettings(team1.getRoundData(), "list1,");
        recyclerAdapter1 = new MyRecyclerAdapter(team1, R.layout.item_layout);
        recyclerView1.setAdapter(recyclerAdapter1);

        team2.clearList();
        team2.addListItem();
        team2.setTotalScore(0);
        teamNameEditText2.setFocusable(true);
        setListSettings(team2.getRoundData(), "list2,");
        recyclerAdapter2 = new MyRecyclerAdapter(team2, R.layout.item_layout);
        recyclerView2.setAdapter(recyclerAdapter2);

        /*
         *  Reset Team Name Edit Texts
         */
        teamNameEditText1.setText("");
        teamNameEditText2.setText("");
        setTeamSettings("", "team_1");
        setTeamSettings("", "team_2");

        /*
         * Reset Total Score Text Views
         */
        totalText1.setText("0");
        totalText2.setText("0");
        setTeamSettings("0", "total_1");
        setTeamSettings("0", "total_2");

    }

    private void setListSettings(ArrayList<Integer> list, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(key + "size", list.size());

        for (int i = 0; i < list.size(); i++)
            editor.putInt(key + i, list.get(i));

        editor.apply();
    }

    private ArrayList<Integer> getListSettings(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        int size = preferences.getInt(key + "size", 0);
        ArrayList<Integer> list = new ArrayList<>();
        if (size != 0) {
            for (int i = 0; i < size; i++) {
                list.add(i, preferences.getInt(key + i, 0));
            }
        }
        return list;
    }

    private String getTeamSettings(String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        return preferences.getString(key, "");
    }

    private void setTeamSettings(String data, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(key, data);
        editor.apply();
    }

    private void initializeRecycleViews(View rootView) {
//        Display display = getActivity().getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x;
//        int height = size.y;

        InputFilter[] inputFilters = {new InputFilter.LengthFilter(10)};

        recyclerView1 = (RecyclerView) rootView.findViewById(R.id.recyclerView1);
        recyclerAdapter1 = new MyRecyclerAdapter(team1, R.layout.item_layout);
        linearLayoutManager1 = new LinearLayoutManager(getActivity());
        recyclerView1.setLayoutManager(linearLayoutManager1);
        recyclerView1.setAdapter(recyclerAdapter1);
        recyclerView1.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView1.setHasFixedSize(true);

        totalText1 = (TextView) rootView.findViewById(R.id.total_score_1);
        teamNameEditText1 = (EditText) rootView.findViewById(R.id.editText1);

        observer1 = new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                team1Total = team1.getTotalScore();
                totalText1.setText(Integer.toString(team1Total));
            }
        };

        team1.addObserver(observer1);
        teamNameEditText1.setFilters(inputFilters);

        recyclerView2 = (RecyclerView) rootView.findViewById(R.id.recyclerView2);
        recyclerAdapter2 = new MyRecyclerAdapter(team2, R.layout.item_layout);
        linearLayoutManager2 = new LinearLayoutManager(getActivity());
        recyclerView2.setLayoutManager(linearLayoutManager2);
        recyclerView2.setAdapter(recyclerAdapter2);
        recyclerView2.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        recyclerView2.setHasFixedSize(true);

        totalText2 = (TextView) rootView.findViewById(R.id.total_score_2);
        teamNameEditText2 = (EditText) rootView.findViewById(R.id.editText2);

        observer2 = new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                team2Total = team2.getTotalScore();
                totalText2.setText(Integer.toString(team2Total));
            }
        };

        team2.addObserver(observer2);
        teamNameEditText2.setFilters(inputFilters);

        initializeTeamNameEditorActionListener(teamNameEditText1);
        initializeTeamNameEditorActionListener(teamNameEditText2);
        initializeItemTouchHelperCallback(recyclerView1);
        initializeItemTouchHelperCallback(recyclerView2);

    }

    private void initializeTeamNameEditorActionListener(final EditText teamName) {

        teamName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                       if(actionId == EditorInfo.IME_ACTION_DONE) {
                           textView.setEnabled(false);
                       }
                        return false;
                    }
                }
        );
    }

    private void initializeItemTouchHelperCallback(RecyclerView recyclerView) {
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                MyRecyclerAdapter.ViewHolder myViewHolder = (MyRecyclerAdapter.ViewHolder) viewHolder;
                myViewHolder.removeScore();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }
}

