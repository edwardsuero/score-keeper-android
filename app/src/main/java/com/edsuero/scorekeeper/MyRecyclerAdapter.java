package com.edsuero.scorekeeper;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ESuero on 7/14/2015.
 */
public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    /**
     * Data Members
     */
    private Team team;
    private ArrayList<Integer> list;
    private int layout_id;
    private MyRecyclerAdapter.ViewHolder viewHolder;

    /**
     * Constructors
     */
    public MyRecyclerAdapter(Team team, int layout_id) {
        this.team = team;
        this.list = team.getRoundData();
        this.layout_id = layout_id;
    }

    /**
     * Recycler Methods
     */
    @Override
    public MyRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout_id, viewGroup, false);
        viewHolder = new MyRecyclerAdapter.ViewHolder(view);

        return viewHolder;
    }
    @Override
    public void onBindViewHolder(MyRecyclerAdapter.ViewHolder holder, int index) {
        //Log.e("BindViewHolder: ", "Position: " + index);
        viewHolder = holder;
        if(team.getRoundData().get(index) == 0) {
            holder.editText.setText("");
            holder.editText.setFocusable(true);
        }
        else {
            holder.editText.setText(team.getRoundData().get(index).toString());
        }

        holder.editText.setOnEditorActionListener(holder);
        holder.editText.addTextChangedListener(holder);
    }
    @Override
    public int getItemCount() {

        if (team.getRoundData() != null && team.getRoundData().size() != 0)
            return team.getRoundData().size();
        else
            return 0;
    }

    /**
     * Subclass
     */
    class ViewHolder extends RecyclerView.ViewHolder implements TextView.OnEditorActionListener, TextWatcher {

        CardView cardView;
        EditText editText;
        String text = "";

        public ViewHolder(View itemView) {
            super(itemView);
            cardView =(CardView)itemView.findViewById(R.id.cardView);
            editText = (EditText)itemView.findViewById(R.id.editText1);
        }

        public void removeScore(){

            int position = getAdapterPosition();

            if(text.trim().length() == 0) {
                team.setListItem(position, 0);
                editText.setText("");
                notifyItemChanged(position);
            }
            else {
                team.removeListItem(position);
                notifyItemRemoved(position);
            }
            team.calculateTotalScore();
        }

        /**
         * OnEditorActionListener methods
         */
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                int position = getAdapterPosition();
                if (text != null) {
                    if (text.trim().length() > 0) {
                        team.setListItem(position, Integer.parseInt(textView.getText().toString()));
                        int currentLastItemPosition = getItemCount() - 1;
                        if (position == currentLastItemPosition) {
                            team.addListItem();
                            notifyItemInserted(currentLastItemPosition + 1);
                            notifyDataSetChanged();
                        }
                        viewHolder.editText.setFocusable(false);
                    }
                    team.calculateTotalScore();
                }

                InputMethodManager imm = (InputMethodManager)textView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                return true;
            }
            return false;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            text = viewHolder.editText.getText().toString();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            text = viewHolder.editText.getText().toString();
        }

        @Override
        public void afterTextChanged(Editable s) {
            text = viewHolder.editText.getText().toString();
        }

    }
}
