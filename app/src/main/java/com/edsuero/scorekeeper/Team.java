package com.edsuero.scorekeeper;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by ESuero on 7/14/2015.
 */
public class Team extends Observable {

    /**
     * Data Members
     */
    private String teamName;
    private ArrayList<Integer> roundData;
    private int totalScore;

    /**
     * Constructors
     */
    public Team(String teamName)
    {
        this.teamName = teamName;
        roundData = new ArrayList<>();
        addListItem();
        totalScore = 0;
    }

    /**
     * Helper Methods
     */
    void calculateTotalScore() {
        int sum = 0;
        for(int i=0; i < roundData.size(); i++)
            sum += roundData.get(i);
        totalScore = sum;
        setChanged();
        notifyObservers();
    }
    void addListItem() {
        roundData.add(0);
    }
    void addListItem(int score) {
        roundData.add(score);
    }
    void addListItem(int index, int score) {
        roundData.add(index,score);
    }
    void setListItem(int index, int score)
    {
       roundData.set(index,score);
    }
    int getListItem(int index)
    {
        return (int)roundData.get(index);
    }
    void removeListItem(int index){
        roundData.remove(index);
    }
    void clearList()
    {
        roundData.clear();
    }
    String getTeamName(){
        return teamName;
    }
    void setTeamName(String teamName){
        this.teamName = teamName;
    }
    ArrayList<Integer> getRoundData(){
        return roundData;
    }
    void setRoundData(ArrayList<Integer> roundData){
        this.roundData = roundData;
        calculateTotalScore();
        setChanged();
        notifyObservers();
    }
    int getTotalScore(){
        return totalScore;
    }
    void setTotalScore(int totalScore){
        this.totalScore = totalScore;
    }
}
